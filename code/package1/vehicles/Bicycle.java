//knadry Rayane
//#2244933
package package1.vehicles;
public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;
    public String getManufacturer() {
        return this.manufacturer;
    }
    public int getnumberGears() {
        return this.numberGears;
    }
    public double getmaxSpeed() {
        return this.maxSpeed;
    }
    public Bicycle(String manu, int gears, double speed) {
        this.manufacturer = manu;
        this.numberGears = gears;
        this.maxSpeed = speed;
    }
    public String toString() {
        String build = "Manufacturer: " + this.manufacturer + ", Number of Gears: " + this.numberGears + ", MaxSpeed: "
        + this.maxSpeed;
        return build;
    }
}
