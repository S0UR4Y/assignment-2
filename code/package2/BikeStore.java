package package2;
import package1.vehicles.Bicycle;
public class BikeStore{
    public static void main(String[]args){
        Bicycle[] bicycles= new Bicycle[4];
        bicycles[0]=new Bicycle("BMX", 2, 30);
        bicycles[1]=new Bicycle("Car", 8, 300);
        bicycles[2]=new Bicycle("bike", 2, 40);
        bicycles[3]=new Bicycle("Truck", 12, 200);
        for(int i=0;i<bicycles.length;i++){
            System.out.println(bicycles[i]);
        }
    }
}